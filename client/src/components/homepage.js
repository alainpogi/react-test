import React, { useEffect, useState } from "react"

import { gql } from "apollo-boost"
import { graphql } from "react-apollo"

import ReactEcharts from "echarts-for-react"

const getDemoQuery = gql`
  {
    graph {
      id
      title
      xAxis
      seriesA
      seriesB
      data {
        id
        seriesA
        seriesB
      }
    }
  }
`

const Homepage = ({ data: { loading, graph } }) => {
  const [title, setTitle] = useState(null)
  const [seriesA, setSeriesA] = useState([])
  const [seriesB, setSeriesB] = useState([])
  const getOption = {
    title: {
      text: title,
    },
    tooltip: {
      trigger: "axis",
    },
    legend: {
      data: ["Alpha", "Beta"],
    },
    toolbox: {
      feature: {
        saveAsImage: {},
      },
    },
    grid: {
      left: "3%",
      right: "4%",
      bottom: "3%",
      containLabel: true,
    },
    xAxis: [
      {
        type: "category",
        boundaryGap: false,
        data: ["Time", "Time", "Time", "Time", "Time", "Time", "Time"],
      },
    ],
    yAxis: [
      {
        type: "value",
      },
    ],
    series: [
      {
        name: "Alpha",
        type: "line",
        stack: "data",
        areaStyle: { normal: {} },
        data: seriesA,
      },
      {
        name: "Beta",
        type: "line",
        stack: "data",
        areaStyle: { normal: {} },
        data: seriesB,
      },
    ],
  }
  const showData = () => {
    if (graph) {
      const title = graph.map(data => data.title)
      setTitle(title)

      const alphaData = graph.map(data => data.data)
      const seriesA = alphaData.map(data => data.map(data => data.seriesA))
      const seriesB = alphaData.map(data => data.map(data => data.seriesB))
      setSeriesA(seriesA[0])
      setSeriesB(seriesB[0])
    }
  }
  useEffect(() => {
    //eslint-disable-next-line
  }, [])

  return (
    <>
      <ReactEcharts
        option={getOption}
        style={{ height: "350px", width: "100%" }}
        className="react_for_echarts"
      />
      <input
        type="submit"
        onClick={showData}
        name="showdata"
        value="Show Data"
      ></input>
    </>
  )
}

export default graphql(getDemoQuery)(Homepage)
