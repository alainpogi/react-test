import React from "react"
import Link from "gatsby-link"
const menu = () => {
  return (
    <div className="menu-container">
      <ul className="menu-list">
        <li>
          <Link to="/">Home</Link>{" "}
        </li>
        <li>
          <Link to="/about">About</Link>
        </li>
        <li>
          <Link to="/blog">Blog</Link>
        </li>
      </ul>
    </div>
  )
}

export default menu
