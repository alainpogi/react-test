import React from "react"
import "../assets/index.css"
import { ApolloProvider } from "react-apollo"

import Layout from "../components/layout"

import SEO from "../components/seo"
import HomePage from "../components/homepage"

import ApolloClient from "apollo-boost"

//apollo client
const client = new ApolloClient({
  uri: "http://localhost:5000/graphql",
})

const IndexPage = () => (
  <ApolloProvider client={client}>
    <Layout>
      <SEO title="Home" />
      <HomePage />
    </Layout>
  </ApolloProvider>
)

export default IndexPage
