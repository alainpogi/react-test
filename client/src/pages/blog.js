import React from "react"
import Layout from "../components/layout"
const blogpage = ({ data }) => {
  return (
    <Layout>
      <h1>Latest Post</h1>

      {data.allMarkdownRemark.edges.map((post, id) => (
        <div key={id}>
          <h3>{post.node.frontmatter.title}</h3>
          <small>
            {post.node.frontmatter.author} on {post.node.frontmatter.date}
          </small>
        </div>
      ))}
    </Layout>
  )
}

export const pageQuery = graphql`
  query BlogIndexQuery {
    allMarkdownRemark {
      edges {
        node {
          frontmatter {
            title
            path
            date
            author
          }
          excerpt
        }
      }
    }
  }
`
export default blogpage
