const mongoose = require('mongoose')
const Schema = mongoose.Schema


const demoSchema = new Schema({
    title: String,
    xAxis: String,
    seriesA: String,
    seriesB: String
})

module.exports = mongoose.model('Demo', demoSchema)