const mongoose = require('mongoose')
const Schema = mongoose.Schema


const dataSchema = new Schema({
    seriesA: Number,
    seriesB: Number,
})

module.exports = mongoose.model('Data', dataSchema)