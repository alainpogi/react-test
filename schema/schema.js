const graphql = require('graphql')

//models
const Demo = require('../models/demo')
const Data = require('../models/data')
//destructuring
const {GraphQLObjectType, GraphQLString,GraphQLSchema, GraphQLID, GraphQLList, GraphQLInt} = graphql
const _= require('lodash')



const DemoType = new GraphQLObjectType({
    name: 'Graph',
    fields: () => ({
        id: {type: GraphQLID},
        title: {type: GraphQLString},
        xAxis: {type: GraphQLString},
        seriesA: {type: GraphQLString},
        seriesB: {type: GraphQLString},
        data: {
            type: new GraphQLList(DataType),
            resolve(parent, args){
                return Data.find({})
            }
        }
    })
})

const DataType = new GraphQLObjectType({
    name:'Data',
    fields: () => ({
        id: {type: GraphQLID},
        seriesA: {type: GraphQLInt},
        seriesB: {type: GraphQLInt}
    })
})

const RootQuery = new GraphQLObjectType({
    name: 'RootQueryType',
    fields: {
        graph: {
            type: new GraphQLList(DemoType),
            resolve(parent, args){
                return Demo.find({})
            }
        },
        data: {
            type: new GraphQLList(DataType),
            resolve(parent, args){
                return Data.find({})
            }
        }
    }
})


//Mutations CRUD
const Mutation = new GraphQLObjectType({
    name: 'Mutation',
    fields: {
        addDemo: {
            type: DemoType,
            args: {
                title: {type: GraphQLString},
                xAxis: {type: GraphQLString},
                seriesA: {type: GraphQLString},
                seriesB: {type: GraphQLString},
            },
            resolve(parent, args){
                const demo = new Demo({
                    title: args.title,
                    xAxis: args.xAxis,
                    seriesA: args.seriesA,
                    seriesB: args.seriesB
                })
                return demo.save()
            }
        },
        addData: {
            type: DataType,
            args: {
                seriesA: {type: GraphQLInt},
                seriesB: {type: GraphQLInt},
            },
            resolve(parent, args){
                const data = new Data({
                    seriesA: args.seriesA,
                    seriesB: args.seriesB
                })
                return data.save()
            }
        }
    }
})


module.exports = new GraphQLSchema({
    query: RootQuery,
    mutation: Mutation
})